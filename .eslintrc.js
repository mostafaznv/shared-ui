module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    extends: ['@nuxtjs', 'plugin:nuxt/recommended'],
    plugins: [],
    rules: {
        indent: ['error', 4, {SwitchCase: 1}],
        'brace-style': ['error', 'stroustrup', {allowSingleLine: true}],
        'function-call-argument-newline': ['error', 'never'],
        'vue/html-indent': ['error', 4],
        'object-curly-spacing': ['error', 'never'],
        'vue/v-slot-style': ['error', {named: 'longform', default: 'v-slot'}],
        'vue/singleline-html-element-content-newline': 'off',
        'vue/max-attributes-per-line': 'off',
        'vue/require-default-prop': 'off',
        'space-before-function-paren': 'off',
        'vue/attributes-order': ['error', {
            order: [
                'DEFINITION',
                'CONDITIONALS',
                'TWO_WAY_BINDING',
                'LIST_RENDERING',
                'RENDER_MODIFIERS',
                'GLOBAL',
                ['UNIQUE', 'SLOT'],
                'OTHER_DIRECTIVES',
                'EVENTS',
                'OTHER_ATTR',
                'CONTENT'
            ],
            alphabetical: false
        }],
        'vue/order-in-components': ['error', {
            order: [
                'name',
                'layout',
                'extends',
                'mixins',
                'validate',
                ['components', 'directives', 'filters'],
                ['props', 'propsData'],
                'data',
                'computed',
                'watch',
                'methods',
                'middleware',
                'watchQuery',
                'fetch',
                'asyncData',
                'head',
                'scrollToTop',
                'transition',
                'LIFECYCLE_HOOKS'
            ]
        }]
    }
}
