export default {
    rules: {
        phone: 'Phone is required.',
        email: 'Email is required',
        username: 'Username can only use letters, numbers and minimum length is 6 characters',
        phoneCorrect: 'Phone syntax is not correct',
        required: 'This field is required',
        numeric: 'The field must be a number',
        digit: 'The field must be a digit',
        codeIsWrong: 'Code is wrong',
        minLength: 'This field must be greater than or equal {min} characters.',
        maxLength: 'This field must be less than {max} characters.',
        exactLength: 'This field must be {length} characters.',
        date: 'This field is not a valid date.',
        exact: 'This field must be {size} characters.',
        min: 'This field must be less than or equal {min}.',
        max: 'This field must be greater than or equal {max}.',
        fileSize: 'This field must be less than {size} megabytes.',
        fileType: 'This field must be a file of type: {types}.'
    }
}
