import DefaultLayout from './components/Layout/DefaultLayout'
import Locale from './locales'

export {DefaultLayout}
export {Locale}
